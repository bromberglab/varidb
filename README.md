usage: varidb [-h] [-q QUERY] [-Q QUERYLIST] [-t TYPE] [-T TOOLS]
              [-r REVISION] [-f FASTA] [-s] [-R REPORT] [-w WILDTYPE]
              [-p POSITION] [-m MUTATION] [-D DB] [-i] [-P PLUGINDIR]
              [-c CREATE] [-o OUTFILE] [-C COLUMNS [COLUMNS ...]]
              [-S SEPARATOR] [-H] [-j] [-F FASTA2SEGUID] [-L [LOGFILE]] [-v]

Query or create variant database

optional arguments:
  -h, --help            show this help message and exit
  -q QUERY, --query QUERY
                        single query: seguid (default), fasta sequence, or any
                        accession id providing --type <db_name>
  -Q QUERYLIST, --querylist QUERYLIST
                        query by list of querys, format: <query>
                        <WT><POS><MUT>
  -t TYPE, --type TYPE  query type: seguid (default) | fasta | <db_name> (e.g.
                        uniprot)
  -T TOOLS, --tools TOOLS
                        name/s of the tool/s (=table name/s) to query
  -r REVISION, --revision REVISION
                        desired entry revision; default: latest
  -f FASTA, --fasta FASTA
                        path to fasta file to use instead of id when using
                        -Q/--querylist
  -s, --split           split result into successful queries and list of
                        missing entries
                        (<outfile_name>_missing.<outfile_suffix>)
  -R REPORT, --report REPORT
                        path to save detailed query report
  -w WILDTYPE, --wildtype WILDTYPE
                        limit query by wildtype(s)
  -p POSITION, --position POSITION
                        limit query by position(s)
  -m MUTATION, --mutation MUTATION
                        limit query by mutation(s)
  -D DB, --db DB        path to sqlite database file; default: varidb.db
  -i, --info            prints database info
  -P PLUGINDIR, --plugindir PLUGINDIR
                        path to plugins directory; default: plugins
  -c CREATE, --create CREATE
                        create new database from path
  -o OUTFILE, --outfile OUTFILE
                        save query result to file
  -C COLUMNS [COLUMNS ...], --columns COLUMNS [COLUMNS ...]
                        columns (in that order) to print/write out; default:
                        all
  -S SEPARATOR, --separator SEPARATOR
                        separator character to use for print/write out;
                        default: ,
  -H, --hideheader      hide header when print/write out; default: False
  -j, --json            format output as json string
  -F FASTA2SEGUID, --fasta2seguid FASTA2SEGUID
                        path to (multi)fasta file - generates seguids
  -L [LOGFILE], --logfile [LOGFILE]
                        redirect logs to file
  -v, --verbose         increase output verbosity

There are 5 possible status values in the output for each query variant:
- ok            (valid variant predictions for the requested variant)
- protein       (the protein is not available in the DB)
- position      (the protein has no position as specified in the query variant)
- wildtype      (the wildtype at the position specified in the query variant does not match the sequence residue in the DB)
- variant       (the prediction for the specified mutation is not available in the DB)
