FROM python:3.8-alpine as base

LABEL maintainer="mmiller@bromberglab.org" \
      description="varidb docker image (https://services.bromberglab.org/varidb)"

FROM base as builder

# setup system
WORKDIR /install
RUN apk --no-cache add git

# setup app
RUN pip install --upgrade pip && pip install --prefix=/install git+https://bitbucket.org/bromberglab/varidb.git#egg=varidb

FROM base
COPY --from=builder /install /usr/local
RUN apk --no-cache add sqlite

# setup bio-node
LABEL bio-node=v1.0

# set environment variables
WORKDIR /app

# set app ENTRYPOINT
ENTRYPOINT ["varidb"]

# set app CMD
CMD ["--help"]
