import re
import csv
import json
import sqlite3
import hashlib
from .helper import import_modules, parse_fasta, get_logger
from .external import seguid
from timeit import default_timer as timer
from datetime import datetime
from collections import OrderedDict
from pathlib import Path


class VariDB:

    def __init__(self, sqlitedb_file: Path, import_path: Path = None, import_targets: list = [], import_skipexisting: bool = False, outfile: Path = None, plugin_dir: Path = None):
        self.log = get_logger(self.__class__.__name__)
        self.sqlitedb_file = sqlitedb_file
        self.sqlitedb_md5 = sqlitedb_file.parent / f'{sqlitedb_file.stem}.md5'
        self.import_path = import_path
        self.import_targets = import_targets
        self.import_skipexisting = import_skipexisting
        self.outfile = outfile
        self.db = None
        self.init_db()
        self.plugins = self.load_plugins(plugin_dir)
        if import_path is not None:
            self.parse_folder()
        self.md5, self.mtime = self.db_info()

    def init_db(self):
        create_db = False
        if self.sqlitedb_file.exists():
            self.log.info(f'SQLite database file = {self.sqlitedb_file.name}')
        else:
            self.log.info(
                f'Creating new SQLite database file: {self.sqlitedb_file.name}'
            )
            create_db = True

        self.db = sqlite3.connect(str(self.sqlitedb_file))
        if create_db:
            cursor = self.db.cursor()
            cursor.execute(
                '''CREATE TABLE sequences (
                seguid text NOT NULL UNIQUE,
                revision int NOT NULL,
                created_at text NOT NULL,
                updated_at text NOT NULL
            );''')
            self.db.commit()

    def load_plugins(self, plugin_dir):
        plugin_dir_verified = plugin_dir if plugin_dir and plugin_dir.exists() else None
        if plugin_dir_verified:
            self.log.debug(f'Loading additional plugins from {plugin_dir.absolute()}')
        return [m.Plugin(self) for m in import_modules('varidb.plugins', folder=plugin_dir_verified)]

    def get_plugin(self, name):
        for p in self.plugins:
            if p.name == name:
                return p
        return None

    def insert_sequence(self, seguid):
        current_date = datetime.now().strftime("%m-%d-%Y_%H-%M-%S")
        cursor = self.db.cursor()
        res = cursor.execute(
            f"SELECT revision FROM sequences WHERE seguid='{seguid}';"
        ).fetchone()
        revision = 0 if res is None else res[0] + 1
        if not revision:
            cursor.execute(
                "INSERT INTO sequences values ("
                + f"'{seguid}',{revision},'{current_date}','{current_date}'"
                + ");")
        else:
            cursor.execute(
                f"UPDATE sequences SET revision={revision}, updated_at='{current_date}' WHERE seguid='{seguid}';"
            )
        self.db.commit()
        rowid = cursor.lastrowid if not revision else cursor.execute(f"SELECT rowid FROM sequences WHERE seguid='{seguid}';").fetchone()[0]
        return rowid, revision

    def check_existing_variants(self, seq_uid, variants, table):
        sqlite_res = self.query_db(seq_uid, tables=[table], qtype='seguid')
        variants_simple = OrderedDict()
        for v in variants:
            pos = v[1]
            variants_simple[pos] = variants_simple.get(pos, []) + [f'{v[0]}{v[1]}{v[2]}']
        variants_existing = set()
        for res in sqlite_res[table]:
            pos = str(res[4])
            var = f'{res[3]}{pos}{res[5]}'
            if pos in variants_simple and var in variants_simple[pos]:
                variants_existing.add(var)
        variants_new = [v for v in variants if f'{v[0]}{v[1]}{v[2]}' not in variants_existing]
        return variants_new

    def query_tables(self):
        cursor = self.db.cursor()
        stmt = "SELECT name FROM sqlite_master WHERE type = 'table';"
        res = cursor.execute(stmt).fetchall()
        return [_[0] for _ in res if _[0] != 'sequences']

    def query_table_details(self, name):
        cursor = self.db.cursor()
        stmt = f'PRAGMA table_info({name});'
        res = cursor.execute(stmt).fetchall()
        return res

    def query_seguids(self):
        cursor = self.db.cursor()
        stmt = "SELECT seguid FROM sequences;"
        res = cursor.execute(stmt).fetchall()
        return [_[0] for _ in res]

    def query2list(self, query, wildtype=None, position=None, mutation=None):
        wildtype = [wildtype] if type(wildtype) == str else wildtype
        position = position.split(',') if type(position) == str else position
        mutation = [mutation] if type(mutation) == str else mutation
        queries = OrderedDict()
        for wt in wildtype if wildtype else []:
            for pos in position if position else []:
                for mut in mutation if mutation else []:
                    queries[f'{wt}{pos}{mut}'] = True
        return {query: queries}

    def query_db(self, query, wildtype=None, position=None, mutation=None, tables=[], qtype='seguid', revision=-1, as_count=False):
        return self.query_handler(query, wildtype, position, mutation, tables, qtype, revision, as_count)

    def query_handler(self, query, wildtype=None, position=None, mutation=None, tables=[], qtype='seguid', revision=-1, as_count=False):
        cursor = self.db.cursor()
        is_tuple = isinstance(query, tuple)
        if qtype == "seguid":
            if is_tuple:
                seguid_cnt = len(query)
                query_tuple = ",".join([f"'{_}'" for _ in query])
                q_id = f"WHERE sequences.seguid IN ({query_tuple}) "
            else:
                seguid_cnt = 1
                q_id = f"WHERE sequences.seguid = '{query}' "
        elif qtype == "fasta":
            records = parse_fasta(query)
            if records:
                seq = OrderedDict(records)
                if len(seq) > 1:
                    seguid_cnt = len(seq)
                    query_tuple = ",".join([f"'{seguid(sequence)}'" for _, sequence in seq.items()])
                    q_id = f"WHERE sequences.seguid IN ({query_tuple}) "
                else:
                    seguid_cnt = 1
                    seguid_query = seguid(seq)
                    q_id = f"WHERE sequences.seguid = '{seguid_query}' "
        else:
            self.log.warning(f'Query by {qtype} not supported at the moment')
            return []

        tables_available = self.query_tables()
        if not tables:
            tables = tables_available

        wildtype_quoted = [f"'{_}'" for _ in wildtype] if wildtype is not None else []
        mutation_quoted = [f"'{_}'" for _ in mutation] if mutation is not None else []
        position_split = position.split(',') if position is not None else []
        
        res_all = {}
        for table in tables:
            if table not in tables_available:
                self.log.warning(f'No such method found in database: {table}')
                continue
            q_wt = f"AND {table}.wildtype in ({', '.join(_ for _ in wildtype_quoted)}) " if wildtype else ""
            q_pos = f"AND {table}.position in ({', '.join(_ for _ in position_split)}) " if position else ""
            q_vars = f"AND {table}.variant in ({', '.join(_ for _ in mutation_quoted)}) " if mutation else ""
            # TODO
            # q_rev = f"AND {table}.revision = (SELECT revision FROM sequences WHERE seguid = '{query}')" if revision == -1 else f"AND revision = {revision}"
            q_rev = ''
            # + f"WHERE sequences.seguid in ({', '.join('?' for _ in q_ids)}) ", q_ids
            all_and_status = f'sequences.seguid, {table}.*, "ok" AS status'
            stmt = f"SELECT {'COUNT(*)' if as_count else all_and_status} FROM sequences " \
                + f"{'' if as_count and all(_ is None for _ in [wildtype, position, mutation]) else f'INNER JOIN {table} ON sequences.rowid = {table}.sequence_id '}" \
                + f"{q_wt}" \
                + f"{q_pos}" \
                + f"{q_vars}" \
                + f"{q_rev}" \
                + f"{q_id}" \
                + ";"
            self.log.debug(stmt if seguid_cnt == 1 else f'{stmt[0:stmt.find("WHERE sequences.seguid")+26]} ... [total: {seguid_cnt}]' )
            timer_start = timer()
            res = cursor.execute(stmt).fetchall()
            query_seconds = timer() - timer_start
            self.log.debug(f'SQLite query for {table} took {query_seconds} seconds')
            res_all[table] = res
        return res_all

    def query_by_instance(self, query, wildtype=None, position=None, mutation=None, tables=[], qtype='seguid', revision=-1):
        sqlite_res = self.query_db(query, wildtype, position, mutation, tables, qtype, revision)
        queries_all_vars = set()
        queries_all_vars.add(query)
        sqlite_parsed = self.query_postprocessing(None, queries_all_vars, {}, sqlite_res)
        return sqlite_parsed

    def query_by_list(self, list_file, query_fasta=None, tables: list = [], qtype: str = 'seguid', revision: int = -1, batch_size=1000):
        records = parse_fasta(query_fasta) if query_fasta else {}
        if query_fasta:
            qtype_checked = 'seguid'
        else:
            qtype_checked = qtype
        if isinstance(list_file, Path):
            pattern_query = re.compile(r'(\S*)\s*(?:([A-Z][0-9]+[A-Z]))?')
            id_mapping, queries, queries_all_vars = self.parse_list(list_file, pattern_query, records)
        else:
            id_mapping = {}
            queries = list_file
            queries_all_vars = set()
            for query_id, subqueries in queries.items():
                if not subqueries:
                    queries_all_vars.add(query_id)

        uids_all = tuple(queries.keys())
        batches = [uids_all[i:i + batch_size] for i in range(0, len(uids_all), batch_size)]
        res = {}
        for batch in batches:
            sqlite_res = self.query_db(batch, qtype=qtype_checked, tables=tables)
            sqlite_parsed = self.query_postprocessing({q: queries[q] for q in queries if q in batch}, queries_all_vars.intersection(set(batch)), id_mapping, sqlite_res)
            for table in sqlite_parsed:
                res[table] = res.get(table, []) + sqlite_parsed[table]

        return res

    def parse_list(self, list_file, pattern_query, records):
        id_mapping = {}
        queries = OrderedDict()
        queries_all_vars = set()
        with list_file.open() as fin:
            for line in fin:
                match = pattern_query.match(line.strip())
                if match:
                    query_id, variant = match.groups()
                    query_id_mapped = records.get(query_id, query_id)
                    if query_id != query_id_mapped:
                        new_id = seguid(query_id_mapped)
                        id_mapping[new_id] = query_id
                        query_id = new_id
                    tmp = queries.get(query_id, OrderedDict())
                    if variant:
                        tmp[variant] = None
                    else:
                        queries_all_vars.add(query_id)
                    queries[query_id] = tmp
                else:
                    self.log.warning(f'Could not parsing entry: {line.strip()}')
        return id_mapping, queries, queries_all_vars

    def query_postprocessing(self, queries, queries_all_vars, id_mapping, sqlite_res):
        post_out = {}
        for table, resultset in sqlite_res.items():
            availability = {}
            if not queries:
                query_id = tuple(queries_all_vars)[0]
                queries = {query_id: {}}
                self.log.debug(f'[{table}] - processing {query_id} [resultset size: {len(resultset)}]')
            else:
                self.log.debug(f'[{table}] - generating availability lookup for {len(queries)} queried sequences [resultset size: {len(resultset)}]')
            for res in resultset:
                query_id = res[0]
                wt, pos, mut = res[3:6]
                if query_id in queries_all_vars or f'{wt}{pos}{mut}' in queries[query_id]:
                    queries[query_id][f'{wt}{pos}{mut}'] = res
                else:
                    continue
                protein_level = availability.get(query_id, {})
                pos_level = protein_level.get(str(pos), {})
                wt_level = pos_level.get(wt, set())
                wt_level.add(mut)
                pos_level[wt] = wt_level
                protein_level[str(pos)] = pos_level
                availability[query_id] = protein_level
            p = self.get_plugin(table)
            if not p:
                self.log.warning(f'Missing plugin: {table}')
                post_out[table] = []
            else:
                post_out[table] = p.parse_resultset(resultset, queries, queries_all_vars, id_mapping, availability)
        return post_out

    def check_status(self, query_id, variant, availability):
        protein_level = availability.get(query_id)
        if protein_level is None:
            return "protein"
        else:
            wt, pos, mut = variant[0], variant[1:-1], variant[-1]
            pos_level = protein_level.get(str(pos))
            if pos_level is None:
                return "position"
            else:
                wt_level = pos_level.get(wt)
                if wt_level is None:
                    return "wildtype"
                else:
                    if mut not in wt_level:
                        return "variant"
                    else:
                        return "ok"

    def parse_folder(self):
        selected_plugins = []
        for p in self.plugins:
            if not self.import_targets or p.name in self.import_targets:
                selected_plugins += [p]

        for p in selected_plugins:
            p.run()
            self.log.debug(f'Processed {p.name} (*.{p.extension})')
        self.db_info(store=True)

    def to_OrderedDict(self, results):
        out = {}
        for tool, tool_res in results.items():
            tool_columns = [_[0] for _ in self.get_plugin(tool).get_prediction_columns()]
            out_tool = []
            for res in tool_res:
                out_tool += [OrderedDict(
                    [
                        ('query', res[0]),
                        ('variant', "".join([str(_) for _ in res[1:4]]))
                    ] +
                    [
                        (tool_columns[i], res[i+4]) for i in range(len(tool_columns))
                    ] +
                    [
                        ('status', res[-1])
                    ]
                )]
            out[tool] = out_tool
        return out

    def as_json(self, results):
        return json.JSONEncoder().encode(self.to_OrderedDict(results))

    def query_db_json(self, query, wildtype=None, position=None, mutation=None, tables=[], qtype='seguid', revision=-1):
        return self.as_json(self.query_db(query, wildtype, position, mutation, tables, qtype, revision))

    def query_db_list_json(self, query, variants, tables=[], qtype='seguid', revision=-1):
        query_in = {}
        queries = OrderedDict()
        for var in variants:
            queries[var] = True
        query_in[query] = queries
        return self.as_json(self.query_by_list(query_in, None, tables, qtype, revision))

    def db_info(self, store=False):
        BLOCK_SIZE = 65536
        db_mtime = self.sqlitedb_file.stat().st_mtime
        db_md5 = hashlib.md5()
        if store or not self.sqlitedb_md5.exists():
            with self.sqlitedb_file.open('rb') as fin, self.sqlitedb_md5.open('w') as fout:
                fb = fin.read(BLOCK_SIZE)
                while len(fb) > 0:
                    db_md5.update(fb)
                    fb = fin.read(BLOCK_SIZE)
                db_md5_hash = db_md5.hexdigest()
                fout.write(f'{db_md5_hash}\n')
        else:
            with self.sqlitedb_md5.open() as fin:
                db_md5_hash = fin.readline().strip()
        if store:
            self.log.debug(f'{self.sqlitedb_file.name}: {db_md5_hash} ({datetime.fromtimestamp(db_mtime).strftime("%m-%d-%Y_%H-%M-%S")})')
        return (db_md5_hash, db_mtime)

    def print_results(self, results, delimiter=",", columns=None, hideheader=False, json_formatted=False):
        delimiter = '\t' if delimiter in ('\\t', 'tab') else delimiter
        if json_formatted:
            print(self.as_json(results))
        else:
            cnt = 0
            for tool, tool_res in results.items():
                cnt += 1
                tool_columns = [_[0] for _ in self.get_plugin(tool).get_prediction_columns()]
                all_columns = ['query', 'variant'] + tool_columns + ['status']
                selected_columns = [_ for _ in columns if _ in all_columns] if columns else all_columns
                if not hideheader:
                    print(f' --- {tool} ---\n{delimiter.join(selected_columns)}')
                for res in tool_res:
                    res_formatted = [res[0], "".join([str(_) for _ in res[1:4]])] + [str(_) for _ in res[4:]]
                    selected_columns_idx = [all_columns.index(_) for _ in selected_columns] if columns else [_ for _ in range(len(all_columns))]
                    formatted = delimiter.join([res_formatted[i] for i in selected_columns_idx])
                    if formatted:
                        print(formatted)
                if cnt < len(results):
                    print('--- end ---')

    def write_results(self, results, delimiter=",", columns=None, hideheader=False, json_formatted=False, split=False, report: Path = None):
        delimiter = '\t' if delimiter in ('\\t', 'tab') else delimiter
        is_multi_out = len(results.keys()) > 1
        results_dict = self.to_OrderedDict(results)
        reports_dict = {}
        if json_formatted:
            with self.outfile.open('w') as fout:
                if split:
                    outfile_missing = self.outfile.parent / f'{self.outfile.stem}_failed{self.outfile.suffix}'
                    for tool, tool_res in results_dict.items():
                        results_dict_ok = {tool: [_ for _ in tool_res if _['status'] == 'ok']}
                        json.dump(results_dict_ok, fout)
                        with outfile_missing.open('w') as fout2:
                            results_dict_missing = {tool: [_ for _ in tool_res if _['status'] != 'ok']}
                            json.dump(results_dict_missing, fout2)
                        if report:
                            report_path = (report.parent / f'{report.stem}_{tool}{report.suffix}') if is_multi_out else report
                            with report_path.open('w') as fout:
                                fout.write(f'success = {len(results_dict_ok[tool])}\nfailed  = {len(results_dict_missing[tool])}\n')
                                reports_dict[tool] = {
                                    'success': len(results_dict_ok[tool]),
                                    'failed': len(results_dict_missing[tool])
                                }
                else:
                    json.dump(results_dict, fout)
                    if report:
                        for tool, tool_res in results_dict.items():
                            results_ok = [_ for _ in tool_res if _['status'] == 'ok']
                            results_missing = [_ for _ in tool_res if _['status'] != 'ok']
                            report_path = (report.parent / f'{report.stem}_{tool}{report.suffix}') if is_multi_out else report
                            with report_path.open('w') as fout:
                                fout.write(f'success = {len(results_ok)}\nfailed  = {len(results_missing)}\n')
                                reports_dict[tool] = {
                                    'success': len(results_ok),
                                    'failed': len(results_missing)
                                }
        else:
            for tool, tool_res in results_dict.items():
                out_path = (self.outfile.parent / f'{self.outfile.stem}_{tool}{self.outfile.suffix}') if is_multi_out else self.outfile
                tool_columns = [_[0] for _ in self.get_plugin(tool).get_prediction_columns()]
                all_columns = ['query', 'variant'] + tool_columns + ['status']
                selected_columns = [_ for _ in columns if _ in all_columns] if columns else all_columns
                skipped_columns = set(all_columns) - set(selected_columns)
                if split:
                    results_ok = [_ for _ in tool_res if _['status'] == 'ok']
                    results_missing = [_ for _ in tool_res if _['status'] != 'ok']
                    if columns:
                        for row in results_ok:
                            for key in skipped_columns:
                                del row[key]
                        for row in results_missing:
                            for key in skipped_columns:
                                del row[key]
                    header_columns = [{_: f'{tool.lower()}.{_}' if _ in tool_columns else _ for _ in selected_columns}]
                    with out_path.open('w') as fout:
                        csv_writer = csv.DictWriter(fout, delimiter=delimiter, fieldnames=selected_columns)
                        if not hideheader:
                            csv_writer.writerows(header_columns)
                        csv_writer.writerows(results_ok)
                    out_path_missing = (self.outfile.parent / f'{self.outfile.stem}_{tool}_failed{self.outfile.suffix}') if is_multi_out else (self.outfile.parent / f'{self.outfile.stem}_failed{self.outfile.suffix}')
                    with out_path_missing.open('w') as fout2:
                        csv_writer = csv.DictWriter(fout2, delimiter=delimiter, fieldnames=selected_columns)
                        if not hideheader:
                            csv_writer.writerows(header_columns)
                        csv_writer.writerows(results_missing)
                else:
                    if report:
                        results_ok = [_ for _ in tool_res if _['status'] == 'ok']
                        results_missing = [_ for _ in tool_res if _['status'] != 'ok']
                    if columns:
                        for row in tool_res:
                            for key in skipped_columns:
                                del row[key]
                    header_columns = [{_: f'{tool.lower()}.{_}' if _ in tool_columns else _ for _ in selected_columns}]
                    with out_path.open('w') as fout:
                        csv_writer = csv.DictWriter(fout, delimiter=delimiter, fieldnames=selected_columns)
                        if not hideheader:
                            csv_writer.writerows(header_columns)
                        csv_writer.writerows(tool_res)
                if report:
                    report_path = (report.parent / f'{report.stem}_{tool}{report.suffix}') if is_multi_out else report
                    with report_path.open('w') as fout:
                        fout.write(f'success = {len(results_ok)}\nfailed  = {len(results_missing)}\n')
                        reports_dict[tool] = {
                                    'success': len(results_ok),
                                    'failed': len(results_missing)
                                }
        if report:
            tools_available = self.query_tables()
            default_columns = ['sequence_id', 'revision', 'wildtype', 'position', 'variant', 'status']
            report_path = report.parent / f'{report.stem}_info{report.suffix}'
            with report_path.open('w') as fout:
                fout.write('# Columns accessible in the variant scoring function\n')
                for tool in tools_available:
                    tool_columns_all = (set([_[1] for _ in self.query_table_details(tool)]))
                    tool_columns = [f'{tool.lower()}.{_}' for _ in (set(tool_columns_all) - set(default_columns))]
                    tool_columns_formatted = '\n'.join([f' - {_}' for  _ in tool_columns])
                    fout.write(f'\n{tool}:\n{tool_columns_formatted}')
            if is_multi_out:
                with report.open('w') as fout:
                    fout.write('# VariDB - variants retrieval statistic\n')
                    for tool in tools_available:
                        fout.write(f'\n{tool}:\n - success = {len(results_ok)}\n - failed  = {len(results_missing)}\n')
