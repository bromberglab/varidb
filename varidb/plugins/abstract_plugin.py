import abc
from pathlib import Path
from collections import OrderedDict
from ..external import seguid
from .. import VariDB


class AbstractPlugin(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, varidb: VariDB, extension: str, name: str):
        self.varidb = varidb
        self.log = self.varidb.log
        self.extension = extension
        self.name = name
        self.outfile = self.varidb.outfile
        self.init_table()

    def get_seguid(self, seq):
        return seguid(seq)

    def insert(self, seq_uid2variants: dict):
        for seq_uid, variants in seq_uid2variants.items():
            if self.varidb.import_skipexisting:
                new_variants = self.varidb.check_existing_variants(seq_uid, variants, self.name)
                if new_variants:
                    seqid, revision = self.varidb.insert_sequence(seq_uid)
                    self.insert_variants(seqid, revision, new_variants)
            else:
                seqid, revision = self.varidb.insert_sequence(seq_uid)
                self.insert_variants(seqid, revision, variants)

    def insert_variants(self, seqid, revision, variants):
        cursor = self.varidb.db.cursor()
        cursor.executemany(
            f'INSERT INTO {self.name} VALUES ({seqid}, {revision}, {", ".join(["?" for _ in variants[0]])});', variants
        )
        self.varidb.db.commit()

    def run(self):
        self.insert(
            self.parse_predictions(self.varidb.import_path)
        )

    def init_table(self):
        cursor = self.varidb.db.cursor()
        stmt = f'CREATE TABLE IF NOT EXISTS {self.name} ('
        for coldefs in self.get_columns():
            stmt += ' '.join(coldefs) + ','
        stmt += ' FOREIGN KEY (sequence_id) REFERENCES sequences (rowid) );'
        cursor.execute(stmt)
        cursor.execute(f'CREATE INDEX IF NOT EXISTS sequence_id_index ON {self.name} (sequence_id);')
        self.varidb.db.commit()

    def get_columns(self):
        default_columns = [
            ('sequence_id', 'integer', 'NOT NULL'),
            ('revision', 'int', 'NOT NULL'),
            ('wildtype', 'text', 'NOT NULL'),
            ('position', 'integer', 'NOT NULL'),
            ('variant', 'integer', 'NOT NULL')
        ]
        return default_columns + self.get_prediction_columns()

    def parse_resultset(self, resultset, queries, queries_all_vars, id_mapping, availability):
        parsed = []
        for query_id, subqueries in queries.items():
            query_id_confirmed = id_mapping.get(query_id, query_id)
            for variant, prediction in subqueries.items():
                if not prediction:
                    status = self.varidb.check_status(query_id, variant, availability)
                    parsed += [
                        (query_id_confirmed, variant[0], variant[1:-1], variant[-1]) +
                        tuple('' for _ in range(len(self.get_prediction_columns()))) +
                        tuple([status])
                    ]
                else:
                    prediction = list(prediction)
                    prediction = tuple([id_mapping.get(query_id, query_id) if id_mapping else prediction[0]] + prediction[3:])
                    parsed += [prediction]
        return parsed

    @abc.abstractmethod
    def get_prediction_columns(self):
        """
        Returns list of columns required to store prediction results.
        For SQLite datatypes see: https://www.sqlite.org/datatype3.html
        For SQLite constraints see: https://www.tutorialspoint.com/sqlite/sqlite_constraints.htm

        Template:
        [
            ('col1_name', 'col1_datatype', 'col1_constraint'),
            ('col2_name', 'col2_datatype', 'col2_constraint')
        ]

        Example:
        [
            ('prediction', 'text', 'NOT NULL'),
            ('score', 'real', 'DEFAULT 0')
        ]
        """
        self.log.warning("not implemented")
        return []

    @abc.abstractmethod
    def parse_predictions(self, datafolder: Path):
        """
        Returns a dictionary of <seguid> -> <list of variants>
        List of variants is a list of tuples (see example)
        Each tuple must contain (in that order): (wildtype, position, mutation[, <...>])
        <...> is a list of values in agreement with columns returned by get_prediction_columns()

        Template (in agreement with get_prediction_columns()):
        {
            'LefCyMgfWvQUaEez5JFZLOHO8tg': [
                (wildtype: str, position: str, mutation: str, prediction: str, score: float),
                (wildtype: str, position: str, mutation: str, prediction: str, score: float)
            ]
        }

        Example (in agreement with get_prediction_columns()):
        {
            'LefCyMgfWvQUaEez5JFZLOHO8tg': [
                ('L', 418 ,'W' , 'effect', 0.4),
                ('M', 418 ,'V' , 'no-effect', -0.9)
            ]
        }
        """
        self.log.warning("not implemented")
        return {}
