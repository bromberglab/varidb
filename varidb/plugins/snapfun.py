import os
import re
import tempfile
import subprocess
from pathlib import Path
from .abstract_plugin import AbstractPlugin
from ..helper import skip_by_pattern, parse_fasta


class Plugin(AbstractPlugin):
    """
    Plugin for SNAPfun output:

    detailed:   M1I =>  11 88 | 13 86 | 12 87 | 14 85 | [...] | sum = -73
    summary:    M1I	 Neutral 		7			94%
    """
    def __init__(self, varidb, extension='snapfun', name='SNAPfun'):
        super().__init__(varidb, extension, name)
        self.pattern_detailed = re.compile(
            r'([A-Z][0-9]+[A-Z])\s+=>\s+(.*)\s+(sum\s=\s)([\-0-9]+)'
        )
        self.pattern_summary = re.compile(
            r'([A-Z][0-9]+[A-Z])\s+([a-z,\-,A-Z]+)\s+([\-0-9]+)\s+([0-9]+%)'
        )

    def get_prediction_columns(self):
        return [
            ('prediction', 'text', 'NOT NULL'),
            ('reliability', 'integer', 'NOT NULL'),
            ('accuracy', 'real', 'NOT NULL'),
            ('score', 'integer', 'NOT NULL')
        ]

    def parse_predictions(self, data_folder):
        self.log.info(f'Scanning for *.snapfun files in {data_folder} ...')
        snap_results = [_ for _ in data_folder.glob(f'*.{self.extension}') if not _.name.startswith('.')]
        records_raw = self.check_fasta(snap_results)
        records = {key.split('.')[0]: val for key, val in records_raw.items()}
        seq_uid2variants = {}
        for snapfun_in in snap_results:
            seq_id = snapfun_in.name.replace('.snapfun', '')
            seq_sequence = records.get(seq_id)
            if seq_sequence is None:
                self.log.warning(f'No UniProt entry found for {seq_id}.')
                continue
            seq_uid = self.get_seguid(seq_sequence)
            if os.stat(snapfun_in).st_size == 0:
                self.log.warning(f'Empty SNAPfun result file for {seq_id}: {snapfun_in}')
                continue
            with snapfun_in.open() as fin:
                variants = []
                skip_by_pattern(r'nsSNP\tPrediction.*', fin)
                for _ in range(2):
                    fin.readline()
                for line in fin:
                    detailed = line.strip()
                    match = self.pattern_detailed.match(detailed)
                    score = match.groups()[-1]
                    summary = fin.readline().strip()
                    match = self.pattern_summary.match(summary)

                    if match:
                        nssnp, prediction, rel_index, exp_acc = match.groups()
                        wt, pos, mut = nssnp[0], nssnp[1:-1], nssnp[-1]
                        variants += [(wt, pos, mut, prediction, int(rel_index), float(exp_acc[:-1]), int(score))]
                    else:
                        self.log.warning(
                            f'Error while parsing {snapfun_in.name}: "{summary}"'
                        )
                seq_uid2variants[seq_uid] = variants
        return seq_uid2variants

    def check_fasta(self, seqids):
        with tempfile.TemporaryDirectory() as tmpdirname:
            records, missing = {}, []
            for snapfun_in in seqids:
                seq_id = snapfun_in.name.replace('.snapfun', '')
                seq_fasta = snapfun_in.parent / f'{seq_id}.fasta'
                if seq_fasta.exists():
                    records_new = parse_fasta(seq_fasta)
                    records = {**records, **records_new}
                else:
                    missing += [seq_id]
            if missing:
                records_new = self.download_fasta(missing, tmpdirname)
                records = {**records, **records_new}
            return records

    def download_fasta(self, uniprot_ids, tmpdirname):
        self.log.info(
            f'Downloading {len(uniprot_ids)} sequence(s) from UniProt...'
        )
        query = Path(tmpdirname) / 'uniprot.fasta'
        n = 100
        uniprot_acs_chunked = [uniprot_ids[i * n:(i + 1) * n] for i in range((len(uniprot_ids) + n - 1) // n)]

        with query.open('w') as fout:
            # single: curl -X GET --header 'Accept:text/x-fasta' 'https://www.ebi.ac.uk/proteins/api/proteins/Q56221'
            # multi:  curl -X GET --header 'Accept:text/x-fasta' 'https://www.ebi.ac.uk/proteins/api/proteins?size=-1&accession=Q56221%2CP03154'
            for idx, uniprot_acs in enumerate(uniprot_acs_chunked, start=1):
                self.log.debug(
                    f'Downloading {idx}/{len(uniprot_acs_chunked)} ...'
                )
                subprocess.call(
                    [
                        "curl", "--silent", "-X", "GET", "--header",
                        "Accept:text/x-fasta", f'https://www.ebi.ac.uk/proteins/api/proteins?size=-1&accession={"%2C".join(uniprot_acs)}'
                    ],
                    shell=False,
                    stdout=fout
                )
        return parse_fasta(query)
