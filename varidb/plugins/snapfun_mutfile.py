import os
import re
import csv
import tempfile
import subprocess
from pathlib import Path
from .abstract_plugin import AbstractPlugin
from ..helper import skip_by_pattern, parse_fasta


class Plugin(AbstractPlugin):
    """
    Plugin for SNAPfun mutout output:

    NM_000028	N13N	-71
    """
    def __init__(self, varidb, extension='txt', name='SNAPfun'):
        super().__init__(varidb, extension, name)
        self.pattern = re.compile(
            r'([A-Z,0-9,_]+)\s([A-Z][0-9]+[A-Z])\s([\-0-9]+)'
        )

    def get_prediction_columns(self):
        return [
            ('prediction', 'text', 'NOT NULL'),
            ('reliability', 'integer', 'NOT NULL'),
            ('accuracy', 'real', 'NOT NULL'),
            ('score', 'integer', 'NOT NULL')
        ]

    def parse_predictions(self, import_path):
        sequences_path = import_path / 'sequences.fasta'
        mutations_path = import_path / 'mutations.txt'
        mapping_path = import_path / 'mapping.tab'
        if not all([path.exists() for path in [sequences_path, mutations_path]]):
            self.log.warn(f'Could not parse mutout from: {import_path}')
            return {}
        self.log.info(f'Reading mutout from {import_path} ...')
        records = parse_fasta(sequences_path)
        mapping = {}
        if mapping_path.exists():
            with mapping_path.open() as fin:
                mapping_reader = csv.reader(fin, delimiter='\t')
                mapping_header = next(mapping_reader)
                for row in mapping_reader:
                    id_mapped = mapping_header.index('Entry')
                    for id_ in row[0].split(','):
                        mapping[id_] = row[id_mapped]
        seq_uid2variants = {}
        with mutations_path.open() as fin:
            ids_processed = {}
            line_cnt = 0
            print(f'Parsing {mutations_path}:')
            for line in fin:
                line_cnt += 1
                if not line_cnt % 1e4:
                    print(f'{line_cnt} ...')
                match = self.pattern.match(line.strip())
                if match:
                    refseq_geneid, nssnp, score = match.groups()
                    if refseq_geneid in ids_processed:
                        id_mapped, seq_uid = ids_processed[refseq_geneid]
                    else:
                        id_mapped = mapping.get(refseq_geneid) if mapping else refseq_geneid
                        seq_sequence = records.get(id_mapped)
                        if seq_sequence is None:
                            self.log.warning(f'No sequence found for {refseq_geneid} [mapping: {id_mapped}].')
                            continue
                        seq_uid = self.get_seguid(seq_sequence)
                        ids_processed[refseq_geneid] = (id_mapped, seq_uid)
                    wt, pos, mut = nssnp[0], nssnp[1:-1], nssnp[-1]
                    score = int(score)
                    prediction = 'Neutral' if score <= 0 else 'Non-neutral'
                    variants = (wt, pos, mut, prediction, -1, -1, score)
                    seq_uid2variants[seq_uid] = seq_uid2variants.get(seq_uid, []) + [variants]
                else:
                    self.log.warning(
                        f'Error while parsing {line.strip()}'
                    )
            print(f'{line_cnt} .ok')
        return seq_uid2variants
