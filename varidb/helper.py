import os
import re
import argparse
import importlib
from pathlib import Path
from .external import seguid
from .logger import Logger

def skip_by_pattern(regexp, fin):
    pattern = re.compile(regexp)
    last_pos = fin.tell()
    line = fin.readline()
    while not pattern.match(line.strip()):
        last_pos = fin.tell()
        line = fin.readline()
        if fin.read(1) == '':
            break
        else:
            fin.seek(fin.tell() - 1, os.SEEK_SET)
    fin.seek(last_pos)

def parse_fasta(fasta_in):
    records = {}
    with open(fasta_in, 'r') as fin:
        header, sequence = None,  ''
        for line in fin:
            line = line.strip()
            if line:
                if line[0] == '>':
                    if sequence:
                        records[header] = sequence
                        sequence = ''
                    if line.find('|') != -1:
                        header = line[1:].split('|')[1]
                    else:
                        header = line[1:]
                else:
                    sequence += line

            if sequence:
                next_line = next(fin, None)
                if next_line is None:
                    records[header] = sequence
                else:
                    next_line = next_line.strip()
                    if next_line:
                        if next_line[0] == '>':
                            records[header] = sequence
                            sequence = ''
                            if next_line.find('|') != -1:
                                header = next_line[1:].split('|')[1]
                            else:
                                header = next_line[1:]
                        else:
                            sequence += next_line
    if sequence:
        records[header] = sequence
    return records

def fasta2seguid(fasta_in: Path, outfile: Path = None):
    fasta_parsed = parse_fasta(fasta_in)
    if outfile:
        with outfile.open('w') as fout:
            for sid, sequence in fasta_parsed.items():
                fout.write(f'{sid},{seguid(sequence)}\n')
    else:
        for sid, sequence in fasta_parsed.items():
            print(f'{sid},{seguid(sequence)}')

def seguid2fasta(fasta_in: Path, seguids, outfile: Path = None):
    if type(seguids) == type(Path()):
        with seguids.open() as fin:
            query_seguids = set(_.strip() for _ in fin.readlines())
    else:
        query_seguids = set(seguids)
    mapped = set()
    fasta_parsed = parse_fasta(fasta_in)
    if outfile:
        with outfile.open('w') as fout:
            for sid, sequence in fasta_parsed.items():
                seguid_ = seguid(sequence)
                if seguid_ in query_seguids:
                    fout.write(f'{sid},{seguid_}\n')
                    mapped.add(seguid_)
    else:
        for sid, sequence in fasta_parsed.items():
            seguid_ = seguid(sequence)
            if seguid_ in query_seguids:
                print(f'{sid},{seguid_}')
                mapped.add(seguid_)
    missing = query_seguids - mapped
    if missing:
        if outfile:
            out_missing = outfile.parent / f'{outfile.stem}_missing.txt'
            print(f'WARNING: Could not map {len(missing)} seguids (see: {out_missing})')
        else:
            out_missing = Path('missing.txt')
            print(f'\n --- WARNING: Could not map {len(missing)} seguids (see: {out_missing}) ---')
        with out_missing.open('w') as fout:
            fout.writelines([f'{_}\n' for _ in missing])

def check_sqlite(*additional_args):
    class customAction(argparse.Action):
        def __call__(self, parser, args, values, option_string=None):
            exists = False
            if args.db.exists():
                exists = True
            elif args.db.with_suffix('.db').exists():
                args.db = args.db.with_suffix('.db')
                exists = True
            elif args.db.with_suffix('.sql').exists():
                args.db = args.db.with_suffix('.sql')
                exists = True
            elif args.db.with_suffix('.sqlite').exists():
                args.db = args.db.with_suffix('.sqlite')
                exists = True

            if not exists and not args.create:
                raise argparse.ArgumentError(
                    self,
                    'No database found. Use -c/--create <IMPORT_FOLDER> to create an new database.'
                )
            setattr(args, self.dest, values)
    return customAction

def get_logger(name):
    logger = Logger(name)
    logger.addLoggers()
    log = logger.getLogger()
    return log

def import_modules(name, package: str = None, folder: Path = None):
    parent_module = importlib.import_module(name, package)
    module_folder = folder if folder else Path(name.replace('.', os.sep))
    if not module_folder.exists():
        module_folder = Path(parent_module.__file__).parent
    modules = []
    for m_ in list(module_folder.glob('**/[!_|__]*.py')):
        if m_.name.startswith('abstract_'):
            continue
        modules += [importlib.import_module(f'{str(folder).replace(".", os.sep)}.{m_.stem}') if folder else importlib.import_module(f'.{m_.stem}', package=name)]
    return modules
