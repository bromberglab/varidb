from .app import VariDB

name = "varidb"
__author__ = 'mmiller'
__version__ = '1.00'
__releasedate__ = '06/01/20'
__all__ = [
    'VariDB',
    'cli'
]
