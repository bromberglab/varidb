#!/usr/bin/env python3

import sys
import argparse
from datetime import datetime
from pathlib import Path
from .app import VariDB
from .helper import check_sqlite, fasta2seguid, seguid2fasta
from .logger import update_logger


def init():
    parser = argparse.ArgumentParser(
        'varidb',
        description='''Query or create variant database''',
        epilog=("There are 5 possible status values in the output for each query variant:\n"
            "- ok		(valid variant predictions for the requested variant)\n"
            "- protein	(the protein is not available in the DB)\n"
            "- position	(the protein has no position as specified in the query variant)\n"
            "- wildtype	(the wildtype at the position specified in the query variant does not match the sequence residue in the DB)\n"
            "- variant	(the prediction for the specified mutation is not available in the DB)\n"
            " \n"),
            formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument('-q', '--query', type=str,
                        help='single query: seguid (default), fasta sequence, or any accession id providing --type <db_name>',
                        action=check_sqlite())
    parser.add_argument('-Q', '--querylist', type=Path,
                        help='query by list of querys, format: <query> <WT><POS><MUT>',
                        action=check_sqlite())
    parser.add_argument('-t', '--type', type=str, default='seguid',
                        help='query type: seguid (default) | fasta | <db_name> (e.g. uniprot)')
    parser.add_argument('-T', '--tools', type=str, action='append',
                        help='name/s of the tool/s (=table name/s) to query')
    parser.add_argument('-b', '--batchsize', type=int, default=1000,
                        help='upper limit of sequences per sqlite query')
    parser.add_argument('-r', '--revision', type=int, default=-1, help='desired entry revision; default: latest')
    parser.add_argument('-f', '--fasta', type=Path,
                        help='path to fasta file to use instead of id when using -Q/--querylist')
    parser.add_argument('-s', '--split', action='store_true',
                        help='split result into successful queries and list of missing entries (<outfile_name>_missing.<outfile_suffix>)')
    parser.add_argument('-R', '--report', type=Path,
                        help='path to save detailed query report')
    parser.add_argument('-w', '--wildtype', type=str,
                        help='limit query by wildtype(s)')
    parser.add_argument('-p', '--position', type=str,
                        help='limit query by position(s)')
    parser.add_argument('-m', '--mutation', type=str,
                        help='limit query by mutation(s)')
    parser.add_argument('-D', '--db', type=Path, default=Path('varidb.db'),
                        help='path to sqlite database file; default: varidb.db')
    parser.add_argument('-i', '--info', action='store_true',
                        help='prints database info')
    parser.add_argument('-P', '--plugindir', type=Path, default=Path('plugins'),
                        help='path to plugins directory; default: plugins')
    parser.add_argument('-c', '--create', type=Path,
                        help='create new database from path')
    parser.add_argument('-e', '--skipexisting', action='store_true',
                        help='do not insert new revisions for variants already available in the database')
    parser.add_argument('-o', '--outfile', type=Path,
                        help='save query result to file')
    parser.add_argument('-C', '--columns', type=str, nargs='+',
                        help='columns (in that order) to print/write out; default: all')
    parser.add_argument('-S', '--separator', type=str, default=',',
                        help='separator character to use for print/write out; default: ,')
    parser.add_argument('-H', '--hideheader', action='store_true',
                        help='hide header when print/write out; default: False')
    parser.add_argument('-j', '--json', action='store_true',
                        help='format output as json string')
    parser.add_argument('-F', '--fasta2seguid', type=Path,
                        help='path to (multi)fasta file - generates seguids')
    parser.add_argument('-U', '--seguid2fasta', type=Path, nargs='+',
                        help='path to: (multi)fasta file to map seguids against; optional: file with seguids (subset of all in db) - returns overlap')
    parser.add_argument('-L', '--logfile', type=Path, const=Path('varidb.log'),
                        nargs='?', help='redirect logs to file')
    parser.add_argument('-v', '--verbose', default=0,
                        action='count', help='increase output verbosity')
    args, _ = parser.parse_known_args()

    args.verbose = 40 - min(10*args.verbose, 40) if args.verbose >= 0 else 0

    update_logger(args.verbose, args.logfile)

    db = VariDB(args.db, args.create, args.tools, args.skipexisting, args.outfile, args.plugindir)
    results = None
    if args.info:
        print(f'{"file":<10}: {db.sqlitedb_file}\n{"version":<10}: {db.md5}\n{"modified":<10}: {datetime.fromtimestamp(db.mtime).strftime("%m-%d-%Y_%H-%M-%S")}')
        sys.exit(0)
    elif args.query:
        results = db.query_by_instance(args.query, args.wildtype, args.position, args.mutation, args.tools, args.type, args.revision)
    elif args.querylist:
        results = db.query_by_list(args.querylist, args.fasta, args.tools, args.type, batch_size=args.batchsize)
    elif args.fasta2seguid:
        fasta2seguid(args.fasta2seguid, args.outfile)
    elif args.seguid2fasta:
        if len(args.seguid2fasta) == 1:
            args.seguid2fasta += [db.query_seguids()]
        seguid2fasta(*args.seguid2fasta, args.outfile)

    if results:
        if not args.outfile:
            db.print_results(results, args.separator, args.columns, args.hideheader, args.json)
        else:
            db.write_results(results, args.separator, args.columns, args.hideheader, args.json, args.split, args.report)

if __name__ == "__main__":
    init()
